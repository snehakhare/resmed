package resMed;

import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.io.BufferedReader;
import java.io.IOException;


public class myGetRequest {

	// https://jsonplaceholder.typicode.com/comments
	public static void getCommentsviaGET() {

	  try {

		URL resourceurl = new URL("https://jsonplaceholder.typicode.com/comments/1");
		HttpURLConnection httpconn = (HttpURLConnection) resourceurl.openConnection();
		httpconn.setRequestMethod("GET");
		httpconn.setRequestProperty("Accept", "application/json");

		if (httpconn.getResponseCode() != 200) {
			throw new RuntimeException("Failed : HTTP error code : "
					+ httpconn.getResponseCode());
		}

		BufferedReader buffreader = new BufferedReader(new InputStreamReader(
			(httpconn.getInputStream())));

		String output;
		System.out.println("Rest service GET Method...Response from server: \n");
		while ((output = buffreader.readLine()) != null) {
			System.out.println(output);
		}

		httpconn.disconnect();

	  } catch (MalformedURLException excep) {

		  excep.printStackTrace();

	  } catch (IOException ioexcep) {

		  ioexcep.printStackTrace();

	  }

	}

}