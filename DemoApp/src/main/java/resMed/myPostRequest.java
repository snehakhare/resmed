package resMed;

import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.io.BufferedReader;
import java.io.IOException;

public class myPostRequest {

	// https://jsonplaceholder.typicode.com/comments
	public static void addPostviaPOST() {

	  try {

		URL resourceurl = new URL("https://jsonplaceholder.typicode.com/posts/");
		HttpURLConnection httpconn = (HttpURLConnection) resourceurl.openConnection();
		httpconn.setDoOutput(true);
		httpconn.setRequestMethod("POST");
		httpconn.setRequestProperty("Content-Type", "application/json");
		
		//String input = "{title: 'My First Post',body: 'My First Post Body',userId: 123}";
		String title = "My First Post";
		String body = "My First Post Body...";
		String resource = "{\"userId\": 10,\"title\": \""+title+"\",\"body\": \""+body+"\"}";
 				 
		OutputStream outs = httpconn.getOutputStream();
		outs.write(resource.getBytes());
		outs.flush();
		
		if (httpconn.getResponseCode() != HttpURLConnection.HTTP_CREATED) {
			throw new RuntimeException("Failed : HTTP error code : "
				+ httpconn.getResponseCode());
		}

		BufferedReader buffreader = new BufferedReader(new InputStreamReader(
				(httpconn.getInputStream())));

		String output;
		System.out.println("Rest service POST Method...Response from server: \n");
		while ((output = buffreader.readLine()) != null) {
			System.out.println(output);
		}

		httpconn.disconnect();

	  } catch (MalformedURLException excep) {

		  excep.printStackTrace();

	  } catch (IOException ioexcep) {

		  ioexcep.printStackTrace();

	 }

	}

}
